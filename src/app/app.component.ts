import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <!--The content below is only a placeholder and can be replaced.-->
      <div class="jumbotron">
        <h1 class="text-center"> {{title}} </h1>
      </div>

      <div class="container">
        <foto url="./assets/img/f100bk.png" titulo="Banana"></foto>
      </div>
    `,

  styles: []
})
export class AppComponent {
  title: string = 'Caelum Pic';
}
