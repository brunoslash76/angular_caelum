import { Component, Input } from '@angular/core'

@Component({
    selector: 'foto',
    template: `
        <img width="300px" src="{{url}}" class="img-responsive" [alt]="titulo">
    `
})
export class FotoComponent {
    @Input() public url: string
    @Input() public titulo: string
}